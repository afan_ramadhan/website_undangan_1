<footer class="main-footer">
	<div class="pull-right hidden-xs">
		[ <b><?=$lihat_konfigurasi['nama_aplikasi'];?></b> <?=$lihat_konfigurasi['versi'];?> ]
	</div>
	Copyright <i class="fa fa-copyright"></i> <?=date("Y");?> <a href="#"><b>KetanWare</b></a>. All rights reserved.
</footer>