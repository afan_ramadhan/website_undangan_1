<?php
include "../../config/database.php";
include "../../libraries/fungsi_waktu.php";

if($_POST['mod']=="editData")
{
	$id = $_POST['id'];
	
	$data = mysql_query("SELECT * FROM user WHERE id = '$id'");
	$getData = mysql_fetch_array($data);
}
?>

<script>
	$(function(){
		$('#tanggal_lahir_').datetimepicker({
			format: 'YYYY-MM-DD',
		});
	})
</script>

<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	<h4 class="modal-title">
		Edit Profil
	</h4>
</div>
<div class="modal-body">
	<table class="table table-hover">
		<tr>
			<td style="border: none;">
				<label class="control-label">Nama Lengkap</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="text" class="form-control" id="nama_lengkap" maxlength="75" value="<?php if($_POST['mod']=="editData"){echo $getData['nama_lengkap'];} ?>"/>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Username</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="text" class="form-control" id="username" maxlength="50" value="<?php if($_POST['mod']=="editData"){echo $getData['username'];} ?>" required/>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Jenis Kelamin</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<select class="form-control" id="jenis_kelamin" required>
					<option value="L" <?php if($_POST['mod']=="editData"){echo ($getData['jenis_kelamin'] == "L" ? "selected" : "");} ?>>Laki - Laki</option>
					<option value="P" <?php if($_POST['mod']=="editData"){echo ($getData['jenis_kelamin'] == "P" ? "selected" : "");} ?>>Perempuan</option>
					
				</select>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Tempat Lahir</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="text" class="form-control" id="tempat_lahir" maxlength="100" value="<?php if($_POST['mod']=="editData"){echo $getData['tempat_lahir'];} ?>" required/>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Tanggal Lahir</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<div class="input-group date" id="tanggal_lahir_">
					<input type="text" class="form-control" id="tanggal_lahir" value="<?php if($_POST['mod']=="editData"){echo $getData['tanggal_lahir'];}else{echo $tanggal_sekarang;} ?>" required/>
					<span class="input-group-addon">
						<span class="fa fa-calendar"></span>
					</span>
				</div>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Agama</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<select class="form-control" id="agama" required>
					<option value="Islam" <?php if($_POST['mod']=="editData"){echo ($getData['agama'] == "Islam" ? "selected" : "");} ?>>Islam</option>
					<option value="Kristen Protestan" <?php if($_POST['mod']=="editData"){echo ($getData['agama'] == "Kristen Protestan" ? "selected" : "");} ?>>Kristen Protestan</option>
					<option value="Katolik" <?php if($_POST['mod']=="editData"){echo ($getData['agama'] == "Katolik" ? "selected" : "");} ?>>Katolik</option>
					<option value="Hindu" <?php if($_POST['mod']=="editData"){echo ($getData['agama'] == "Hindu" ? "selected" : "");} ?>>Hindu</option>
					<option value="Buddha" <?php if($_POST['mod']=="editData"){echo ($getData['agama'] == "Buddha" ? "selected" : "");} ?>>Buddha</option>
					<option value="Kong Hu Cu" <?php if($_POST['mod']=="editData"){echo ($getData['agama'] == "Kong Hu Cu" ? "selected" : "");} ?>>Kong Hu Cu</option>
				</select>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Alamat</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="text" class="form-control" id="alamat" maxlength="200" value="<?php if($_POST['mod']=="editData"){echo $getData['alamat'];} ?>" required/>
			</td>
		</tr>
		
		<tr>
			<td style="border: none;">
				<label class="control-label">Password</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="password" class="form-control" id="password" maxlength="50" value="" required/>
				<p class="help-block">Password Hanya Diisi Apabila Ingin Diganti.</p>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Email</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="text" class="form-control" id="email" maxlength="100" value="<?php if($_POST['mod']=="editData"){echo $getData['email'];} ?>" required/>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Nomor Telepon</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="text" class="form-control" id="nomor_telepon" maxlength="15" value="<?php if($_POST['mod']=="editData"){echo $getData['nomor_telepon'];} ?>" required/>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Foto</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="file" class="form-control" id="gambar" name="gambar"/>
				<p class="help-block">Gambar Harus Berformat jpg / jpeg / png.</p>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Tema</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<select class="form-control" id="tema" required>
					<option value="skin-blue" <?php if($_POST['mod']=="editData"){echo ($getData['tema'] == "skin-blue" ? "selected" : "");} ?>>Skin Blue</option>
					<option value="skin-black" <?php if($_POST['mod']=="editData"){echo ($getData['tema'] == "skin-black" ? "selected" : "");} ?>>Skin Black</option>
					<option value="skin-purple" <?php if($_POST['mod']=="editData"){echo ($getData['tema'] == "skin-purple" ? "selected" : "");} ?>>Skin Purple</option>
					<option value="skin-green" <?php if($_POST['mod']=="editData"){echo ($getData['tema'] == "skin-green" ? "selected" : "");} ?>>Skin Green</option>
					<option value="skin-red" <?php if($_POST['mod']=="editData"){echo ($getData['tema'] == "skin-red" ? "selected" : "");} ?>>Skin Red</option>
					<option value="skin-yellow" <?php if($_POST['mod']=="editData"){echo ($getData['tema'] == "skin-yellow" ? "selected" : "");} ?>>Skin Yellow</option>
					<option value="skin-blue-light" <?php if($_POST['mod']=="editData"){echo ($getData['tema'] == "skin-blue-light" ? "selected" : "");} ?>>Skin Blue Light</option>
					<option value="skin-black-light" <?php if($_POST['mod']=="editData"){echo ($getData['tema'] == "skin-black-light" ? "selected" : "");} ?>>Skin Black Light</option>
					<option value="skin-purple-light" <?php if($_POST['mod']=="editData"){echo ($getData['tema'] == "skin-purple-light" ? "selected" : "");} ?>>Skin Purple Light</option>
					<option value="skin-green-light" <?php if($_POST['mod']=="editData"){echo ($getData['tema'] == "skin-green-light" ? "selected" : "");} ?>>Skin Green Light</option>
					<option value="skin-red-light" <?php if($_POST['mod']=="editData"){echo ($getData['tema'] == "skin-red-light" ? "selected" : "");} ?>>Skin Red Light</option>
					<option value="skin-yellow-light" <?php if($_POST['mod']=="editData"){echo ($getData['tema'] == "skin-yellow-light" ? "selected" : "");} ?>>Skin Yellow Light</option>
				</select>
			</td>
		</tr>
	</table>
</div>
<div class="modal-footer">
	<button type="button" class="btn btn-success" id="perbaruiData" onclick="perbaruiData(<?=$getData['id'];?>)"><i class="fa fa-save" aria-hidden="true" style="margin-right: 10px;"></i>Perbarui</button>
</div>