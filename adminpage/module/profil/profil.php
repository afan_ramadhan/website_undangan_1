<script>
	//Ganti Title
	function GantiTitle()
	{
		document.title="<?=$lihat_konfigurasi['nama_aplikasi'];?> <?=$lihat_konfigurasi['versi'];?> Manajemen | Profil";
	}
	GantiTitle();
		
	//Tampil Database
	$(document).ready(function(){
		$('#tampilData').load("module/profil/profil_database.php");
	})
		
	//Edit Data
	function editData(id)
	{
		var mod = "editData";
		var id = id;
		$.ajax({
			type	: "POST",
			url		: "module/profil/profil_form.php",
			data	: "mod=" + mod +
					  "&id=" + id,
			success: function(html)
			{
				$("#formContent").html(html);
				$("#form").modal();
			}
		})
	}
		
	function perbaruiData(id)
	{
		mulaiAnimasi();
		var data = new FormData();
		data.append("mod", "perbaruiData");
		data.append("id", id);
		data.append("nama_lengkap", $("#nama_lengkap").val());
		data.append("username", $("#username").val());
		data.append("jenis_kelamin", $("#jenis_kelamin").val());
		data.append("tempat_lahir", $("#tempat_lahir").val());
		data.append("tanggal_lahir", $("#tanggal_lahir").val());
		data.append("agama", $("#agama").val());
		data.append("alamat", $("#alamat").val());
		data.append("password", $("#password").val());
		data.append("email", $("#email").val());
		data.append("nomor_telepon", $("#nomor_telepon").val());
		data.append("gambar", $('#gambar')[0].files[0]);
		data.append("tema", $("#tema").val());	
		$.ajax({
			type		: "POST",
			url			: "module/profil/profil_action.php",
			data		: data,
			cache		: false,
			processData	: false,
			contentType	: false,
			success: function(html)
			{
				stopAnimasi();
				$("#notifikasi").html(html);
				$("#tampilData").load("module/profil/profil_database.php");
			}
		})
	}
</script>

<div class="content-wrapper">
	<section class="content-header">
		<h1>Profil</h1>
		<ol class="breadcrumb">
			<li><a href="index.php"><i class="fa fa-chevron-right" style="margin-right: 10px;"></i>Dashboard</a></li>
			<li class="active">Profil</li>
		</ol>
	</section>
	<section class="content container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-default">
					<div class="box-header with-border">
						<h3 class="box-title">Data Profil</h3>
					</div>
					<div class="box-body">
						<button type="button" class="btn btn-warning btn-md" onclick="editData(<?=$_SESSION['id'];?>)" style="margin-right: 10px;"><i class="fa fa-pencil" aria-hidden="true" style="margin-right: 10px;"></i>Edit Profil</button>	
						<div class="modal fade" id="form" role="dialog">
							<div class="modal-dialog">
								<div id="formContent" class="modal-content"></div>
								<div id="notifikasi"></div>
							</div>
						</div>			
						<br/>
						<br/>  
						<div id="tampilData" class="scrolling" onload="tampilData()" style="padding-bottom: 45px;"></div>
					</div>
				</div>
			</div>
		</div>
	</section>	
</div>