<?php
$hakAkses = mysql_query("SELECT user.id AS id_user, level.id AS id_level, hak_akses.id_menu, menu.nama_menu, hak_akses.r, hak_akses.s FROM user LEFT JOIN level ON user.id_level = level.id RIGHT JOIN hak_akses ON level.id = hak_akses.id_level LEFT JOIN menu ON hak_akses.id_menu = menu.id WHERE user.id = '$_SESSION[id]'");
while($getHakAkses = mysql_fetch_array($hakAkses))
{
	$dashboardWidget[$getHakAkses['nama_menu']]['r'] = $getHakAkses['r'];
	$dashboardWidget[$getHakAkses['nama_menu']]['s'] = $getHakAkses['s'];
}

$jumlahPesan = mysql_num_rows(mysql_query("SELECT * FROM pesan"));
$jumlahLevel = mysql_num_rows(mysql_query("SELECT * FROM level"));
$jumlahUser = mysql_num_rows(mysql_query("SELECT * FROM user"));
$jumlahSaran = mysql_num_rows(mysql_query("SELECT * FROM saran"));

function background($skin)
{
	if($skin == "skin-blue" or $skin == "skin-blue-light")
	{
		return "bg-aqua";
	}
	else if($skin == "skin-black" or $skin == "skin-black-light")
	{
		return "bg-gray";
	}
	else if($skin == "skin-purple" or $skin == "skin-purple-light")
	{
		return "bg-fuchsia";
	}
	else if($skin == "skin-green" or $skin == "skin-green-light")
	{
		return "bg-lime";
	}
	else if($skin == "skin-red" or $skin == "skin-red-light")
	{
		return "bg-maroon";
	}
	else if($skin == "skin-yellow" or $skin == "skin-yellow-light")
	{
		return "bg-red";
	}
}
?>

<div class="content-wrapper">
	<section class="content-header">
		<h1>Dashboard</h1>
		<ol class="breadcrumb">
			<li class="active"><i class="fa fa-chevron-right" style="margin-right: 10px;"></i>Dashboard</li>
		</ol>
	</section>
	<section class="content container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-default">
					<div class="box-header with-border">
						<h3 class="box-title">Total Data</h3>
					</div>
					<div class="box-body">
						
						<div class="row">
						
							<div class="col-lg-3 col-xs-6" style="<?=($dashboardWidget['pesan']['r'] == 1 ? "" : "display: none;");?>">
								<div class="small-box <?=background($ambilTema[0]);?>">
									<div class="inner">
										<h3><?=$jumlahPesan;?></h3>
										<p>Total Pesan</p>
									</div>
									<div class="icon">
										<i class="fa fa-envelope-o"></i>
									</div>
									<a href="?mod=pesan" class="small-box-footer">Lebih Detail<i class="fa fa-arrow-circle-right" style="margin-left: 10px;"></i></a>
								</div>
							</div>
							
							<div class="col-lg-3 col-xs-6" style="<?=($dashboardWidget['level']['r'] == 1 ? "" : "display: none;");?>">
								<div class="small-box <?=background($ambilTema[0]);?>">
									<div class="inner">
										<h3><?=$jumlahLevel;?></h3>
										<p>Total Level</p>
									</div>
									<div class="icon">
										<i class="fa fa-diamond"></i>
									</div>
									<a href="?mod=level" class="small-box-footer">Lebih Detail<i class="fa fa-arrow-circle-right" style="margin-left: 10px;"></i></a>
								</div>
							</div>
							
							<div class="col-lg-3 col-xs-6" style="<?=($dashboardWidget['user']['r'] == 1 ? "" : "display: none;");?>">
								<div class="small-box <?=background($ambilTema[0]);?>">
									<div class="inner">
										<h3><?=$jumlahUser;?></h3>
										<p>Total User</p>
									</div>
									<div class="icon">
										<i class="fa fa-user-o"></i>
									</div>
									<a href="?mod=user" class="small-box-footer">Lebih Detail<i class="fa fa-arrow-circle-right" style="margin-left: 10px;"></i></a>
								</div>
							</div>
							
							<div class="col-lg-3 col-xs-6" style="<?=($dashboardWidget['saran']['r'] == 1 ? "" : "display: none;");?>">
								<div class="small-box <?=background($ambilTema[0]);?>">
									<div class="inner">
										<h3><?=$jumlahSaran;?></h3>
										<p>Total Saran</p>
									</div>
									<div class="icon">
										<i class="fa fa-commenting-o"></i>
									</div>
									<a href="?mod=saran" class="small-box-footer">Lebih Detail<i class="fa fa-arrow-circle-right" style="margin-left: 10px;"></i></a>
								</div>
							</div>
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>