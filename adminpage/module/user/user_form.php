<?php
include "../../config/database.php";
include "../../libraries/fungsi_waktu.php";

if($_POST['mod']=="editData")
{
	$id = $_POST['id'];
	
	$data = mysql_query("SELECT * FROM user WHERE id = '$id'");
	$getData = mysql_fetch_array($data);
}
?>

<script>
	$(function(){
		$('#tanggal_lahir_').datetimepicker({
			format: 'YYYY-MM-DD',
		});
	})
</script>

<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	<h4 class="modal-title">
		<?php if($_POST['mod'] == "editData"){echo "Edit User";}else{echo "Tambah User";} ?>
	</h4>
</div>
<div class="modal-body">
	<table class="table table-hover">
		<tr>
			<td style="border: none;">
				<label class="control-label">Nama Lengkap</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="text" class="form-control" id="nama_lengkap" maxlength="75" value="<?php if($_POST['mod']=="editData"){echo $getData['nama_lengkap'];} ?>"/>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Username</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="text" class="form-control" id="username" maxlength="50" value="<?php if($_POST['mod']=="editData"){echo $getData['username'];} ?>" required/>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Password</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="password" class="form-control" id="password" maxlength="50" value="" required/>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Jenis Kelamin</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<select class="form-control" id="jenis_kelamin" required>
					<option value="L" <?php if($_POST['mod']=="editData"){echo ($getData['jenis_kelamin'] == "L" ? "selected" : "");} ?>>Laki - Laki</option>
					<option value="P" <?php if($_POST['mod']=="editData"){echo ($getData['jenis_kelamin'] == "P" ? "selected" : "");} ?>>Perempuan</option>
					
				</select>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Tempat Lahir</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="text" class="form-control" id="tempat_lahir" maxlength="100" value="<?php if($_POST['mod']=="editData"){echo $getData['tempat_lahir'];} ?>" required/>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Tanggal Lahir</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<div class="input-group date" id="tanggal_lahir_">
					<input type="text" class="form-control" id="tanggal_lahir" value="<?php if($_POST['mod']=="editData"){echo $getData['tanggal_lahir'];}else{echo $tanggal_sekarang;} ?>" required/>
					<span class="input-group-addon">
						<span class="fa fa-calendar"></span>
					</span>
				</div>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Agama</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<select class="form-control" id="agama" required>
					<option value="Islam" <?php if($_POST['mod']=="editData"){echo ($getData['agama'] == "Islam" ? "selected" : "");} ?>>Islam</option>
					<option value="Kristen Protestan" <?php if($_POST['mod']=="editData"){echo ($getData['agama'] == "Kristen Protestan" ? "selected" : "");} ?>>Kristen Protestan</option>
					<option value="Katolik" <?php if($_POST['mod']=="editData"){echo ($getData['agama'] == "Katolik" ? "selected" : "");} ?>>Katolik</option>
					<option value="Hindu" <?php if($_POST['mod']=="editData"){echo ($getData['agama'] == "Hindu" ? "selected" : "");} ?>>Hindu</option>
					<option value="Buddha" <?php if($_POST['mod']=="editData"){echo ($getData['agama'] == "Buddha" ? "selected" : "");} ?>>Buddha</option>
					<option value="Kong Hu Cu" <?php if($_POST['mod']=="editData"){echo ($getData['agama'] == "Kong Hu Cu" ? "selected" : "");} ?>>Kong Hu Cu</option>
					
				</select>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Alamat</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="text" class="form-control" id="alamat" maxlength="200" value="<?php if($_POST['mod']=="editData"){echo $getData['alamat'];} ?>" required/>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Email</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="text" class="form-control" id="email" maxlength="100" value="<?php if($_POST['mod']=="editData"){echo $getData['email'];} ?>" required/>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Nomor Telepon</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="text" class="form-control" id="nomor_telepon" maxlength="15" value="<?php if($_POST['mod']=="editData"){echo $getData['nomor_telepon'];} ?>" required/>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Level</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<select class="form-control" id="id_level" required>
					<?php
					$level = mysql_query("SELECT * FROM level ORDER BY nama_level");
					while($getLevel = mysql_fetch_array($level))
					{
						$selected = ($getData['id_level'] == $getLevel['id'] ? "selected" : "");
					?>
						<option value="<?=$getLevel['id'];?>" <?=$selected;?>><?=$getLevel['nama_level'];?></option>
					<?php
					}
					?>
				</select>
			</td>
		</tr>
		
		<?php
		if($_POST['mod'] == "editData")
		{
		?>
			<tr>
				<td style="border: none;">
					<label class="control-label">Blokir</label>
				</td>
				<td style="border: none;"><label class="control-label">:</label></td>
				<td style="border: none;">
					<input type="radio" name="blokir" value="Y" style="margin-right: 10px;" <?php if($getData['blokir'] == "Y"){echo "checked";} ?>> Ya
					<input type="radio" name="blokir" value="N" style="margin-left: 20px; margin-right: 10px;" <?php if($getData['blokir'] == "N"){echo "checked";} ?>> Tidak
				</td>
			</tr>
		<?php
		}
		?>
		
	</table>
</div>
<div class="modal-footer">
	<?php
	if($_POST['mod']=="editData")
	{
		echo "<button type='button' class='btn btn-success' id='perbaruiData' onclick='perbaruiData($getData[id])'><i class='fa fa-save' aria-hidden='true' style='margin-right: 10px;'></i>Perbarui</button>";
	}
	else
	{
		echo "<button type='button' class='btn btn-success' id='simpanData' onclick='simpanData()'><i class='fa fa-save' aria-hidden='true' style='margin-right: 10px;'></i>Simpan</button>";
	}
	?>
</div>