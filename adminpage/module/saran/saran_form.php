<?php
session_start();
include "../../config/database.php";
include "../../libraries/fungsi_waktu.php";
include "../../libraries/fungsi_user_agent.php";

$nama_menu = "saran";
$hakAkses = mysql_query("SELECT user.id AS id_user, level.id AS id_level, hak_akses.id_menu, menu.nama_menu, hak_akses.s FROM user LEFT JOIN level ON user.id_level = level.id RIGHT JOIN hak_akses ON level.id = hak_akses.id_level LEFT JOIN menu ON hak_akses.id_menu = menu.id WHERE user.id = '$_SESSION[id]' AND nama_menu = '$nama_menu'");
$getHakAkses = mysql_fetch_array($hakAkses);

$s = $getHakAkses['s'];

if($_POST['mod']=="editData")
{
	$id = $_POST['id'];
	
	$data = mysql_query("SELECT * FROM saran WHERE id = '$id'");
	$getData = mysql_fetch_array($data);
}
else
{
	$id = 0;
}
?>

<script src="assets/plugins/tinymce/tinymce.min.js" type="text/javascript"></script>	

<script>
	tinymce.init({
		selector: '.textEditor',
		height: 200,
		menubar: false,
		statusbar: false,
		toolbar: ' removeformat | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist ',
	});
	
	$(document).on('focusin', function(e){
		if ($(event.target).closest(".mce-window").length){
			e.stopImmediatePropagation();
		}
	});
</script>

<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	<h4 class="modal-title">
		<?php if($_POST['mod'] == "editData"){echo "Edit Saran";}else{echo "Tambah Saran";} ?>
	</h4>
</div>
<div class="modal-body">
	<table class="table table-hover">
		<tr>
			<td style="border: none;">
				<label class="control-label">Saran</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<textarea id="saran" class="textEditor"><?php if($_POST['mod']=="editData"){echo $getData['saran'];} ?></textarea>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Penilaian Aplikasi</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<select class="form-control" id="penilaian_aplikasi" required>
					<option value="1" <?php if($_POST['mod']=="editData"){echo ($getData['penilaian_aplikasi'] == 1 ? "selected" : "");} ?>>Kurang Memuaskan</option>
					<option value="2" <?php if($_POST['mod']=="editData"){echo ($getData['penilaian_aplikasi'] == 2 ? "selected" : "");} ?>>Cukup Memuaskan</option>
					<option value="3" <?php if($_POST['mod']=="editData"){echo ($getData['penilaian_aplikasi'] == 3 ? "selected" : "");}else{echo "selected";} ?>>Memuaskan</option>
					<option value="4" <?php if($_POST['mod']=="editData"){echo ($getData['penilaian_aplikasi'] == 4 ? "selected" : "");} ?>>Sangat Memuaskan</option>
				</select>
			</td>
		</tr>
	</table>
</div>
<div class="modal-footer">
	<?php
	if($_POST['mod']=="editData")
	{
		echo "<button type='button' class='btn btn-success' id='perbaruiData' onclick='perbaruiData($getData[id])'><i class='fa fa-save' aria-hidden='true' style='margin-right: 10px;'></i>Perbarui</button>";
	}
	else
	{
		echo "<button type='button' class='btn btn-success' id='simpanData' onclick='simpanData()'><i class='fa fa-save' aria-hidden='true' style='margin-right: 10px;'></i>Simpan</button>";
	}
	?>
</div>