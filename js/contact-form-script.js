$("#contactForm").validator().on("submit", function (event) {
    if (event.isDefaultPrevented()) {
        // handle the invalid form...
        formError();
        // submitMSG(false, "Did you fill in the form properly?");
        submitMSG(false, "Pastikan Kotak Isian Telah Diisi Dengan Benar");
    } else {
        // everything looks good!
        event.preventDefault();
        submitForm();
    }
});


function submitForm(){
    // Initiate Variables With Form Content
    var nama_lengkap = $("#nama_lengkap").val();
    var alamat = $("#alamat").val();
    var telepon = $("#telepon").val();
    var kehadiran = $("#kehadiran").val();
    var pesan = $("#pesan").val();

    $.ajax({
        type: "POST",
        url: "php/form-process.php",
        data: "nama_lengkap=" + nama_lengkap + "&alamat=" + alamat + "&telepon=" + telepon + "&kehadiran=" + kehadiran + "&pesan=" + pesan,
        success : function(text){
            if (text == "success"){
                formSuccess();
            } else {
                formError();
                submitMSG(false,text);
            }
        }
    });
}

function formSuccess(){
    $("#contactForm")[0].reset();
    submitMSG(true, "Message Submitted!")
}

function formError(){
    $("#contactForm").removeClass().addClass('shake animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
        $(this).removeClass();
    });
}

function submitMSG(valid, msg){
    if(valid){
        var msgClasses = "h3 text-center tada animated text-success";
    } else {
        var msgClasses = "h3 text-center text-danger";
    }
    $("#msgSubmit").removeClass().addClass(msgClasses).text(msg);
}