<?php
include "config.php";
?>

<!DOCTYPE html>

<html lang="en">

	<head>

		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">   
		<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
	 
		<title><?=$title;?></title> 
		
		<meta name="keywords" content="<?=$keywords;?>">
		<meta name="description" content="<?=$description;?>">
		<meta name="author" content="<?=$author;?>">

		<link rel="shortcut icon" href="images/favicon.png" type="image/x-icon">
		<link rel="apple-touch-icon" href="images/favicon.png">

		<link rel="stylesheet" href="css/bootstrap.min.css">
		<link rel="stylesheet" href="css/pogo-slider.min.css">
		<link rel="stylesheet" href="css/style.css">    
		<link rel="stylesheet" href="css/responsive.css">
		<link rel="stylesheet" href="css/custom.css">
		
		<link rel="stylesheet" href="plugins/font-awesome/css/font-awesome.min.css"/>

		<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		  <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
		
		
		<style>
			.top-header .navbar .navbar-collapse ul li a {
				padding: 19px 10px;
				text-transform: capitalize;
				font-size: 17px;
			}
			
			.custom-font-1 {
				font-family: 'Engagement', cursive;
				font-size: 30px;
			}
			
			.custom-font-2 {
				font-weight: bold;
			}
			
			#toggle-button {
				position: fixed;
				bottom: 15px;
				right: 15px;
				z-index: 9999;
			}
			
			.pesan {
				margin-top: 10px;
				font-size: 20px;
			}
			
			.tanggal-waktu-pesan {
				font-size: 14px;
			}
		</style>
		
	</head>

	<body id="beranda" data-spy="scroll" data-target="#navbar-wd" data-offset="98">
	
	
		<div id="preloader">
			<div class="preloader pulse">
				<h3><?=$pengantin_1;?> & <?=$pengantin_2;?></h3>
			</div>
		</div>
		
		
		<header class="top-header">
			<nav class="navbar header-nav navbar-expand-lg">
				<div class="container">
					<a class="navbar-brand" href="index.php"><img src="images/logo.png" alt="image"></a>
					<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-wd" aria-controls="navbar-wd" aria-expanded="false" aria-label="Toggle navigation">
						<span></span>
						<span></span>
						<span></span>
					</button>
					<div class="collapse navbar-collapse justify-content-end" id="navbar-wd">
						<ul class="navbar-nav" style="">
							<li><a class="nav-link active" href="#beranda">Beranda</a></li>
							<li><a class="nav-link" href="#profil-pengantin">Profil Pengantin</a></li>
							<li><a class="nav-link" href="#keluarga">Keluarga</a></li>
							<li><a class="nav-link" href="#galeri">Galeri</a></li>
							<li><a class="nav-link" href="#acara">Acara</a></li>
							<li><a class="nav-link" href="#lokasi">Lokasi</a></li>
							<li><a class="nav-link" href="#protokol-kesehatan">Protokol Kesehatan</a></li>
							<li><a class="nav-link" href="#ucapan-doa">Ucapan Dan Do'a</a></li>
						</ul>
					</div>
				</div>
			</nav>
		</header>
		
		
		<div class="home-slider">
			<ul class="rslides">
				<li><img src="images/slider-01.jpg" alt=""></li>
				<li><img src="images/slider-02.jpg" alt=""></li>
				<li><img src="images/slider-03.jpg" alt=""></li>
			</ul>
			<div class="lbox-details">
				<h1><?=$pengantin_1;?> & <?=$pengantin_2;?></h1>
				<h2><?=$sambutan_undangan;?></h3>
				<div class="countdown main-time clearfix">
					<div id="timer">
						<h3><?=$tanggal;?></h3>
						<div id="days"></div>
						<div id="hours"></div>
						<div id="minutes"></div>
						<div id="seconds"></div>
					</div>
				</div>
			</div>
		</div>
		
		
		<div id="profil-pengantin" class="about-box">
			<div class="about-a1">
				<div class="container">
					<div class="row">
						<div class="col-lg-12 text-center">
							<div class="title-box">
								<h2><?=$pengantin_1;?> <span> & </span> <?=$pengantin_2;?></h2>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12 col-md-12 col-sm-12">
							<div class="row align-items-center about-main-info">
								<div class="col-lg-4 col-md-6 col-sm-12">
									<div class="about-m">
										<div class="about-img">
											<img class="img-fluid" src="images/about-img-01.jpg" alt="" />
										</div>
										<ul>
											<?=($facebook_pengantin_1 == "" ? "" : "<li><a href='$facebook_pengantin_1'><i class='fa fa-facebook'></i></a></li>");?>
											<?=($twitter_pengantin_1 == "" ? "" : "<li><a href='$twitter_pengantin_1'><i class='fa fa-twitter'></i></a></li>");?>
											<?=($instagram_pengantin_1 == "" ? "" : "<li><a href='$instagram_pengantin_1'><i class='fa fa-instagram'></i></a></li>");?>
										</ul>
									</div>
								</div>
								<div class="col-lg-8 col-md-6 col-sm-12">
									<h2> <i class="fa fa-heart-o" aria-hidden="true"></i> <span><?=$pengantin_1;?></span> <i class="fa fa-heart-o" aria-hidden="true"></i></h2>
									<p><?=$profil_pengantin_1;?></p>
								</div>
							</div>
							<div class="row align-items-center about-main-info">
								<div class="col-lg-4 col-md-6 col-sm-12">
									<div class="about-m">
										<div class="about-img">
											<img class="img-fluid" src="images/about-img-02.jpg" alt="" />
										</div>
										<ul>
											<?=($facebook_pengantin_2 == "" ? "" : "<li><a href='$facebook_pengantin_2'><i class='fa fa-facebook'></i></a></li>");?>
											<?=($twitter_pengantin_2 == "" ? "" : "<li><a href='$twitter_pengantin_2'><i class='fa fa-twitter'></i></a></li>");?>
											<?=($instagram_pengantin_2 == "" ? "" : "<li><a href='$instagram_pengantin_2'><i class='fa fa-instagram'></i></a></li>");?>
										</ul>
									</div>
								</div>
								<div class="col-lg-8 col-md-6 col-sm-12">
									<h2> <i class="fa fa-heart-o" aria-hidden="true"></i> <span><?=$pengantin_2;?></span> <i class="fa fa-heart-o" aria-hidden="true"></i></h2>
									<p><?=$profil_pengantin_2;?></p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		
		<div id="keluarga" class="family-box">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="title-box">
							<h2>Keluarga</h2>
							<p><?=$keluarga_ket;?></p>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-4 col-md-6 col-sm-12">
						<div class="single-team-member">
							<div class="family-img">
								<img class="img-fluid" src="images/family-01.jpg" alt="" />
							</div>
							<div class="family-info">
								<h4>Abu Fulan</h4>
								<p>{ Bapak Dari Fulan }</p>
							</div>
						</div>
					</div>
					<div class="col-lg-4 col-md-6 col-sm-12">
						<div class="single-team-member">
							<div class="family-img">
								<img class="img-fluid" src="images/family-02.jpg" alt="" />
							</div>
							<div class="family-info">
								<h4>Ummu Fulan</h4>
								<p>{ Ibu Dari Fulan }</p>
							</div>
						</div>
					</div>
					<div class="col-lg-4 col-md-6 col-sm-12">
						<div class="single-team-member">
							<div class="family-img">
								<img class="img-fluid" src="images/family-03.jpg" alt="" />
							</div>
							<div class="family-info">
								<h4>Akhi Fulan</h4>
								<p>{ Saudara Laki-Laki Fulan }</p>
							</div>
						</div>
					</div>
					<div class="col-lg-4 col-md-6 col-sm-12">
						<div class="single-team-member">
							<div class="family-img">
								<img class="img-fluid" src="images/family-04.jpg" alt="" />
							</div>
							<div class="family-info">
								<h4>Ukhty Fulan</h4>
								<p>{ Saudari Perempuan Fulan }</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		
		<div id="galeri" class="gallery-box">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="title-box">
							<h2>Galeri</h2>
							<p>Koleksi Foto Galeri <?=$pengantin_1;?> Dan <?=$pengantin_2;?></p>
						</div>
					</div>
				</div>
				<div class="row">
					<ul class="popup-gallery clearfix">
						<li>
							<a href="images/gallery-01.jpg">
								<img class="img-fluid" src="images/gallery-01.jpg" alt="single image">
								<span class="overlay"><i class="fa fa-heart-o" aria-hidden="true"></i></span>
							</a>
						</li>
						<li>
							<a href="images/gallery-02.jpg">
								<img class="img-fluid" src="images/gallery-02.jpg" alt="single image">
								<span class="overlay"><i class="fa fa-heart-o" aria-hidden="true"></i></span>
							</a>
						</li>
						<li>
							<a href="images/gallery-03.jpg">
								<img class="img-fluid" src="images/gallery-03.jpg" alt="single image">
								<span class="overlay"><i class="fa fa-heart-o" aria-hidden="true"></i></span>
							</a>
						</li>
						<li>
							<a href="images/gallery-04.jpg">
								<img class="img-fluid" src="images/gallery-04.jpg" alt="single image">
								<span class="overlay"><i class="fa fa-heart-o" aria-hidden="true"></i></span>
							</a>
						</li>
						<li>
							<a href="images/gallery-05.jpg">
								<img class="img-fluid" src="images/gallery-05.jpg" alt="single image">
								<span class="overlay"><i class="fa fa-heart-o" aria-hidden="true"></i></span>
							</a>
						</li>
						<li>
							<a href="images/gallery-06.jpg">
								<img class="img-fluid" src="images/gallery-06.jpg" alt="single image">
								<span class="overlay"><i class="fa fa-heart-o" aria-hidden="true"></i></span>
							</a>
						</li>
						<li>
							<a href="images/gallery-07.jpg">
								<img class="img-fluid" src="images/gallery-07.jpg" alt="single image">
								<span class="overlay"><i class="fa fa-heart-o" aria-hidden="true"></i></span>
							</a>
						</li>
						<li>
							<a href="images/gallery-08.jpg">
								<img class="img-fluid" src="images/gallery-08.jpg" alt="single image">
								<span class="overlay"><i class="fa fa-heart-o" aria-hidden="true"></i></span>
							</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
		
		
		<div id="acara" class="wedding-box">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="title-box">
							<h2>Acara</h2>
							<p><?=$acara_ket;?></p>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6 col-sm-6">
						<div class="serviceBox">
							<div class="service-icon"><i class="fa fa-handshake-o" aria-hidden="true"></i></div>
							<h3 class="title"><?=$acara_1_ket;?></h3>
							<h4><?=$tanggal_waktu_acara_1;?></h4>
							<p class="description">
								<?=$lokasi_acara_1;?>
							</p>
						</div>
					</div>
					<div class="col-md-6 col-sm-6">
						<div class="serviceBox">
							<div class="service-icon"><i class="flaticon-wedding"></i></div>
							<h3 class="title"><?=$acara_2_ket;?></h3>
							<h4><?=$tanggal_waktu_acara_2;?></h4>
							<p class="description">
								<?=$lokasi_acara_2;?>
							</p>
						</div>
					</div>
				</div>
				
			</div>
		</div>
		
		
		<div id="lokasi" class="events-box">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="title-box">
							<h2>Lokasi</h2>
							<p><?=$lokasi;?></p>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12">
						<div class="event-inner">
							<div class="event-img">
								<iframe src="<?=$lokasi_src;?>" width="100%" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
							</div>
							<a href="<?=$lokasi_href;?>">Buka Di Google Maps <i class="fa fa-map-marker" aria-hidden="true"></i></a>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		
		<div id="protokol-kesehatan" class="story-box main-timeline-box">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="title-box">
							<h2>Protokol Kesehatan</h2>
							<p>Mohon Untuk Para Tamu Undangan Untuk Tetap Memperhatikan Pprotokol Kesehatan Yaitu :</p>
						</div>
					</div>
				</div>
				<div class="timeLine">
					<div class="row">
						<div class="lineHeader hidden-sm hidden-xs"></div>
						<div class="lineFooter hidden-sm hidden-xs"></div>
						
						<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 item" >
							<div class="caption">
								<div class="star center-block">
									<span class="h3">01</span>
								</div>
								<div class="image">
									<img src="images/protokol-kesehatan-01.jpg"/>
									<div class="title">
										<h2>Menggunakan Masker</h2>
									</div>
								</div>
							</div>
						</div>
						
						<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 item">
							<div class="caption">
								<div class="star center-block">
									<span class="h3">02</span>
								</div>
								<div class="image">
									<img src="images/protokol-kesehatan-02.jpg"/>
									<div class="title">
										<h2>Mencuci Tangan</h2>
									</div>
								</div>
							</div>
						</div>
						
						<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 item">
							<div class="caption">
								<div class="star center-block">
									<span class="h3">03</span>
								</div>
								<div class="image">
									<img src="images/protokol-kesehatan-03.jpg"/>
									<div class="title">
										<h2>Menjaga Jarak</h2>
									</div>
								</div>
							</div>
						</div>
						
						<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 item">
							<div class="caption">
								<div class="star center-block">
									<span class="h3">04</span>
								</div>
								<div class="image">
									<img src="images/protokol-kesehatan-04.jpg"/>
									<div class="title">
										<h2>Menghindari Kerumunan</h2>
									</div>
								</div>
							</div>
						</div>
						
					</div>
				</div>
			</div>
		</div>
		
		
		<div id="ucapan-doa" class="contact-box">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="title-box">
							<h2>Ucapan Dan Do'a</h2>
							<p><?=$doa_ket;?></p>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12 col-sm-12 col-xs-12">
					  <div class="contact-block">
						<form id="contactForm">
						  <div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<input type="text" class="form-control" id="nama_lengkap" name="nama_lengkap" maxlength="75" placeholder="Nama Lengkap" required data-error="Silahkan Masukan Nama Lengkap">
									<div class="help-block with-errors"></div>
								</div>                                 
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<input type="text" class="form-control" id="alamat" name="alamat" maxlength="200" placeholder="Alamat" required data-error="Silahkan Masukan Alamat">
									<div class="help-block with-errors"></div>
								</div>                                 
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<input type="text" class="form-control" id="telepon" name="telepon" maxlength="15" placeholder="Nomor Telepon (Opsional)">
									<div class="help-block with-errors"></div>
								</div>                                 
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<select class="custom-select d-block form-control" id="kehadiran" required data-error="Silahkan Pilih Opsi Kehadiran.">
										<option value="" disabled selected>Pilih Kehadiran :</option>
										<option value="1">Insyaallah Hadir</option>
										<option value="0">Maaf Belum Bisa Hadir</option>
									</select>
									<div class="help-block with-errors"></div>
								</div> 
							</div>
							<div class="col-md-12">
								<div class="form-group"> 
									<textarea class="form-control" id="pesan" placeholder="Berikan Ucapan Dan Do'a.. ." rows="8" data-error="Silahkan Berikan Ucapan Dan Do'a" required></textarea>
									<div class="help-block with-errors"></div>
								</div>
								<div class="submit-button text-center">
									<button class="btn btn-common" id="submit" type="submit">Kirim</button>
									<div id="msgSubmit" class="h3 text-center hidden" style="margin-top: 20px;"></div> 
									<div class="clearfix"></div> 
								</div>
							</div>
						  </div>            
						</form>
					  </div>
					</div>
				</div>
			</div>
		</div>
		
		
		<hr/>
		
		
		<div id="doa-pesan" class="contact-box">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="title-box">
							<h2>Terima Kasih Atas Do'anya</h2>
						</div>
					</div>
				</div>
				<div class="panel panel-default widget">
					<div class="panel-body" style="max-height: 500px; overflow-x: auto;">
						<ul class="list-group">
							
							<?php
							$dataPesan = mysql_query("SELECT * FROM pesan ORDER BY id_pesan DESC");
							while($ambilDataPesan = mysql_fetch_array($dataPesan))
							{
							?>
							
								<li class="list-group-item">
									<div class="row">
										<div class="col-xs-12 col-md-12">
											<div class="pengirim">
												<a href="#doa-pesan"><?=$ambilDataPesan['nama_lengkap'];?></a>
												<div class="mic-info">
													Di: <a href="#doa-pesan"><?=$ambilDataPesan['alamat'];?></a>
												</div>
											</div>
											<div class="pesan">
												<i>"<?=$ambilDataPesan['pesan'];?>"</i>
											</div>
											<div class="tanggal-waktu-pesan">
												<i class="fa fa-calendar-o" aria-hidden="true" style="margin-right: 10px;"></i><?=tanggal_indonesia($ambilDataPesan['tanggal_dikirim']);?>
											</div>
										</div>
									</div>
								</li>
							
							<?php
							}
							?>
						</ul>
					</div>
				</div>
			</div>
		</div>
		
		
		<footer class="footer-box">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<p class="footer-company-name">Mau Bikin Website Undangan Juga? Silahkan Pesan Di : <a href="https://wa.me/6289609918353">Ketanware</a></p>
					</div>
				</div>
			</div>
		</footer>
		
		
		<div class="modal fade" id="sambutan" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title custom-font-1">Assalamu'alaikum Warohmatullahi Wabarokatuh.. .</h4>
					</div>
					<div class="modal-body">
						<table>
							<tr>
								<td class="custom-font-2" align="right">Teruntuk</td>
								<td class="custom-font-2" style="padding-left: 10px; padding-right: 10px;">:</td>
								<td class="custom-font-2"><?=$penerima;?></td>
							</tr>
							<tr>
								<td class="custom-font-2" align="right">Di</td>
								<td class="custom-font-2" style="padding-left: 10px; padding-right: 10px;">:</td>
								<td class="custom-font-2"><?=$alamat;?></td>
							</tr>
						</table>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default btn-sm" data-dismiss="modal" onclick="playAudio()">Tutup</button>
					</div>
				</div>
			</div>
		</div>
		
		
		<audio id="opening" controls style="display: none;">
			<source src="audio/murrotal.mp3" type="audio/mpeg">
			Your browser does not support the audio element.
		</audio>
		
		
		<div id="toggle-button">
			<button class="btn btn-warning" style="border-radius: 45px;" onclick="toggleAudio()"><i id="toggle-icon" class="fa fa-pause" aria-hidden="true"></i></button>
		</div>
		
		
		<script src="js/jquery.min.js"></script>
		<script src="js/popper.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/jquery.magnific-popup.min.js"></script>
		<script src="js/jquery.pogo-slider.min.js"></script> 
		<script src="js/slider-index.js"></script>
		<script src="js/smoothscroll.js"></script>
		<script src="js/responsiveslides.min.js"></script>
		<script src="js/timeLine.min.js"></script>	
		<script src="js/form-validator.min.js"></script>
		<script src="js/contact-form-script.js"></script>
		<script src="js/custom.js"></script>
		
		<script>
			$.when($("#preloader").fadeOut("slow")).done(function(){
				$("#sambutan").modal();
			});
			
			var audio = document.getElementById("opening"); 
			
			function playAudio(){
				audio.play();
			}
			
			function toggleAudio(){
				var cekStatus = audio.paused;
				
				if(cekStatus == false)
				{
					audio.pause();
					$("#toggle-icon").removeClass("fa-pause");
					$("#toggle-icon").addClass("fa-play");
				}
				else
				{
					audio.play();
					$("#toggle-icon").removeClass("fa-play");
					$("#toggle-icon").addClass("fa-pause");
				}
			}
			
			document.getElementById("opening").addEventListener("ended", myHandler, false);
			
			function myHandler(e){
				$("#toggle-icon").removeClass("fa-pause");
				$("#toggle-icon").addClass("fa-play");
			}
		</script>
		
	</body>

</html>